package ru.tsc.babeshko.tm.command;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.babeshko.tm.api.model.ICommand;
import ru.tsc.babeshko.tm.api.service.IServiceLocator;
import ru.tsc.babeshko.tm.enumerated.Role;

public abstract class AbstractCommand implements ICommand {

    @NotNull
    protected IServiceLocator serviceLocator;

    public void setServiceLocator(@NotNull final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @NotNull
    public abstract String getName();

    @NotNull
    public abstract String getDescription();

    @Nullable
    public abstract Role[] getRoles();

    public abstract void execute();

    @Nullable
    public String getArgument() {
        return null;
    }

    @Nullable
    protected String getToken() {
        return serviceLocator.getTokenService().getToken();
    }

    protected void setToken(@NotNull final String token) {
        serviceLocator.getTokenService().setToken(token);
    }

    @Override
    public String toString() {
        String result = "";
        @NotNull final String name = getName();
        @Nullable final String argument = getArgument();
        @NotNull final String description = getDescription();
        if (!name.isEmpty()) result += name + " : ";
        if (argument != null && !argument.isEmpty()) result += argument + " : ";
        if (!description.isEmpty()) result += description;
        return result;
    }

}