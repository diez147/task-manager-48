package ru.tsc.babeshko.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.tsc.babeshko.tm.api.service.IConnectionService;
import ru.tsc.babeshko.tm.api.service.IPropertyService;
import ru.tsc.babeshko.tm.api.service.dto.IProjectServiceDTO;
import ru.tsc.babeshko.tm.api.service.dto.IProjectTaskServiceDTO;
import ru.tsc.babeshko.tm.api.service.dto.ITaskServiceDTO;
import ru.tsc.babeshko.tm.api.service.dto.IUserServiceDTO;
import ru.tsc.babeshko.tm.dto.model.ProjectDTO;
import ru.tsc.babeshko.tm.dto.model.TaskDTO;
import ru.tsc.babeshko.tm.dto.model.UserDTO;
import ru.tsc.babeshko.tm.enumerated.Status;
import ru.tsc.babeshko.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.babeshko.tm.exception.field.EmptyDescriptionException;
import ru.tsc.babeshko.tm.exception.field.EmptyIdException;
import ru.tsc.babeshko.tm.exception.field.EmptyNameException;
import ru.tsc.babeshko.tm.exception.field.EmptyUserIdException;
import ru.tsc.babeshko.tm.model.Project;
import ru.tsc.babeshko.tm.service.dto.ProjectServiceDTO;
import ru.tsc.babeshko.tm.service.dto.ProjectTaskServiceDTO;
import ru.tsc.babeshko.tm.service.dto.TaskServiceDTO;
import ru.tsc.babeshko.tm.service.dto.UserServiceDTO;
import ru.tsc.babeshko.tm.util.DateUtil;

import java.util.List;
import java.util.UUID;

public class TaskServiceTest {

    @NotNull
    private IUserServiceDTO userService;

    @NotNull
    private ITaskServiceDTO taskService;

    @NotNull
    private IProjectServiceDTO projectService;

    @NotNull
    private IProjectTaskServiceDTO projectTaskService;

    private String userId;

    private String taskId;

    private long initSize;

    @Before
    public void init() {
        @NotNull final IPropertyService propertyService = new PropertyService();
        @NotNull final IConnectionService connectionService = new ConnectionService(propertyService);
        taskService = new TaskServiceDTO(connectionService);
        projectService = new ProjectServiceDTO(connectionService);
        projectTaskService = new ProjectTaskServiceDTO(connectionService, projectService, taskService);
        userService = new UserServiceDTO(connectionService, propertyService, taskService, projectService);
        @NotNull final UserDTO user = userService.create("user", "user");
        userId = user.getId();
        @NotNull final TaskDTO task = taskService.create(userId, "test-1");
        taskId = task.getId();
        initSize = taskService.getCount();
    }

    @After
    public void end() {
        taskService.clear(userId);
        userService.removeByLogin("user");
        projectService.clear(userId);
    }

    @Test
    public void create() {
        Assert.assertThrows(EmptyUserIdException.class, () -> taskService.create("", "test"));
        Assert.assertThrows(EmptyNameException.class, () -> taskService.create(userId, ""));
        taskService.create(userId, "test");
        Assert.assertEquals(initSize + 1, taskService.getCount());
    }

    @Test
    public void createWithDescription() {
        Assert.assertThrows(EmptyUserIdException.class, () -> taskService.create("", "test", "test"));
        Assert.assertThrows(EmptyNameException.class, () -> taskService.create(userId, "", "test"));
        Assert.assertThrows(EmptyDescriptionException.class, () -> taskService.create(userId, "test", ""));
        taskService.create(userId, "test", "test");
        Assert.assertEquals(initSize + 1, taskService.getCount());
    }

    @Test
    public void createWithDescriptionAndDate() {
        @Nullable final TaskDTO task = taskService.create(
                userId,
                "test",
                "test",
                DateUtil.toDate("10.10.2021"),
                DateUtil.toDate("11.11.2021")
        );
        Assert.assertEquals(initSize + 1, taskService.getCount());
        Assert.assertNotNull(task.getDateBegin());
        Assert.assertNotNull(task.getDateEnd());
    }

    @Test
    public void clear() {
        taskService.clear();
        Assert.assertEquals(0, taskService.getCount());
    }

    @Test
    public void findAll() {
        @NotNull final List<TaskDTO> tasksAll = taskService.findAll();
        Assert.assertEquals(initSize, tasksAll.size());
        @NotNull final List<TaskDTO> tasksOwnedUser1 = taskService.findAll(userId);
        Assert.assertEquals(1, tasksOwnedUser1.size());
        @NotNull final List<TaskDTO> tasksOwnedUser2 = taskService.findAll(UUID.randomUUID().toString());
        Assert.assertEquals(0, tasksOwnedUser2.size());
    }

    @Test
    public void updateById() {
        Assert.assertThrows(EmptyUserIdException.class,
                () -> taskService.updateById("", taskId, "test", "test"));
        Assert.assertThrows(EmptyIdException.class,
                () -> taskService.updateById(userId, "", "test", "test"));
        Assert.assertThrows(EmptyNameException.class,
                () -> taskService.updateById(userId, taskId, "", "test"));
        @NotNull final String newName = "new name";
        @NotNull final String newDescription = "new description";
        taskService.updateById(userId, taskId, newName, newDescription);
        @Nullable final TaskDTO task = taskService.findOneById(taskId);
        Assert.assertEquals(newName, task.getName());
        Assert.assertEquals(newDescription, task.getDescription());
    }

    @Test
    public void changeTaskStatusById() {
        @NotNull final Status newStatus = Status.COMPLETED;
        Assert.assertThrows(EmptyUserIdException.class,
                () -> taskService.changeStatusById("", taskId, newStatus));
        Assert.assertThrows(EmptyIdException.class,
                () -> taskService.changeStatusById(userId, "", newStatus));
        taskService.changeStatusById(userId, taskId, newStatus);
        @Nullable final TaskDTO task = taskService.findOneById(taskId);
        Assert.assertNotNull(task.getStatus());
        Assert.assertEquals(newStatus, task.getStatus());
    }

    @Test
    public void findOneById() {
        @NotNull final String taskName = "test find by id";
        @NotNull final TaskDTO task = taskService.create(userId, taskName);
        @NotNull final String taskId = task.getId();
        Assert.assertThrows(EmptyIdException.class, () -> taskService.findOneById(""));
        Assert.assertNotNull(taskService.findOneById(taskId));
        Assert.assertEquals(taskName, taskService.findOneById(taskId).getName());
        Assert.assertNotNull(taskService.findOneById(userId, taskId));
        Assert.assertEquals(taskName, taskService.findOneById(userId, taskId).getName());
    }

    @Test
    public void existsById() {
        @NotNull final String taskName = "test exist by id";
        @NotNull final TaskDTO task = taskService.create(userId, taskName);
        @NotNull final String taskId = task.getId();
        Assert.assertTrue(taskService.existsById(taskId));
        Assert.assertFalse(taskService.existsById(UUID.randomUUID().toString()));
    }

    @Test
    public void remove() {
        @NotNull final TaskDTO task = taskService.create(userId, "test");
        taskService.remove(task);
        Assert.assertEquals(initSize, taskService.getCount());
        taskService.add(task);
        taskService.remove(userId, task);
        Assert.assertEquals(initSize, taskService.getCount());
    }

    @Test
    public void removeById() {
        @NotNull final TaskDTO task = taskService.create(userId, "test");
        @NotNull final String taskId = task.getId();
        Assert.assertThrows(EmptyIdException.class, () -> taskService.removeById(""));
        taskService.removeById(taskId);
        Assert.assertEquals(initSize, taskService.getCount());
        taskService.add(task);
        Assert.assertThrows(EmptyIdException.class, () -> taskService.removeById(userId, ""));
        taskService.removeById(userId, taskId);
        Assert.assertEquals(initSize, taskService.getCount());
    }

    @Test
    public void removeAllByProjectId() {
        @NotNull final TaskDTO task = taskService.create(userId, "test");
        @NotNull final TaskDTO task2 = taskService.create(userId, "test2");
        @NotNull final ProjectDTO project = projectService.create(userId, "test_proj");
        @NotNull final String projectId = project.getId();
        @NotNull final String taskId = task.getId();
        @NotNull final String task2Id = task2.getId();
        Assert.assertEquals(initSize + 2, taskService.getCount());
        projectTaskService.bindTaskToProject(userId, project.getId(),taskId);
        projectTaskService.bindTaskToProject(userId, project.getId(),task2Id);
        Assert.assertThrows(EmptyUserIdException.class, () -> taskService.removeAllByProjectId("",""));
        Assert.assertThrows(EmptyUserIdException.class, () -> taskService.removeAllByProjectId("",projectId));
        Assert.assertThrows(ProjectNotFoundException.class, () -> taskService.removeAllByProjectId(userId,""));
        taskService.removeAllByProjectId(userId, projectId);
        Assert.assertEquals(initSize, taskService.getCount());
    }

    @Test
    public void removeAllByUserId() {
        taskService.create(userId, "test");
        taskService.create(userId, "test2");
        Assert.assertEquals(initSize + 2, taskService.getCount());
        Assert.assertThrows(EmptyUserIdException.class, () -> taskService.removeAllByUserId(""));
        taskService.removeAllByUserId(userId);
        Assert.assertEquals(0, taskService.getCount());
    }

}