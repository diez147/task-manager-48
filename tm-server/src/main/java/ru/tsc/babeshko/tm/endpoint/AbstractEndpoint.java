package ru.tsc.babeshko.tm.endpoint;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.babeshko.tm.api.service.IServiceLocator;
import ru.tsc.babeshko.tm.dto.request.AbstractUserRequest;
import ru.tsc.babeshko.tm.enumerated.Role;
import ru.tsc.babeshko.tm.exception.system.AccessDeniedException;
import ru.tsc.babeshko.tm.dto.model.SessionDTO;

@NoArgsConstructor
public abstract class AbstractEndpoint {

    @Getter
    @Nullable
    private IServiceLocator serviceLocator;

    public AbstractEndpoint(@NotNull final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @NotNull
    protected SessionDTO check(@Nullable final AbstractUserRequest request) {
        if (request == null) throw new AccessDeniedException();
        @Nullable final String token = request.getToken();
        if (token == null || token.isEmpty()) throw new AccessDeniedException();
        return getServiceLocator().getAuthService().validateToken(token);
    }

    @NotNull
    protected SessionDTO check(@Nullable final AbstractUserRequest request, @Nullable final Role role) {
        if (request == null) throw new AccessDeniedException();
        if (role == null) throw new AccessDeniedException();
        @Nullable final String token = request.getToken();
        if (token == null || token.isEmpty()) throw new AccessDeniedException();
        @NotNull SessionDTO session = getServiceLocator().getAuthService().validateToken(token);
        if (session.getRole() == null) throw new AccessDeniedException();
        if (!session.getRole().equals(role)) throw new AccessDeniedException();
        return session;
    }

}