package ru.tsc.babeshko.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.babeshko.tm.enumerated.Role;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public class Session extends AbstractModel {

    @Nullable
    private User user;

    @NotNull
    private Date date = new Date();

    @Nullable
    private Role role = null;

}